#!/bin/bash
#
# Manage the different shutdown type with a timer
#
# Copyright (c) 2011, Benoit RAT
# Authors: Benoit RAT <benoit(AT)sevensols.com>
# Licence: GPL v2 or later.
# Website: http://www.neub.co.nr
#
#######################



### Show help of the function
help () 
{
cat << EOF
Usage: $(basename $0)  [Options] <action> [+minutes]

action:
	suspend | halt | shutdown |  hibernate | cancel
	
Options:	
	-?|--help)     	 Show this little help
	-i|--inactive) disable the inactive option
	
EOF
exit 0
}


#################################################
### Main execution

# setup script dir
scriptdir=$(cd $(dirname $0); pwd)
op=0

while [ $# -gt 0 ]; do    # Until you run out of parameters . . .
	case "$1" in
	-?|--help)     	 	help;;
	-i|--inactive) 	f_d=1;;
	-*) echo "Bad option $1"; help; exit;;
	*) break;;
	esac
	shift       # Check next set of parameters.
done

# Set default actions
action="$1"
if [ -z $action ]; then
	action="suspend";
	minutes="90";
	echo "Default action is $action in $minutes m";
elif [ "$1" -eq "$1" ] 2>/dev/null; then
	action="suspend"
	minutes="$1"
else
	minutes="$2"
fi

# Set number of minutes
if [ -z $minutes ]; then 
	minutes="0"
	echo "Action is doing in now"
fi


if [ "x$action" = "xsuspend" ]; then
	echo "suspend in $minutes m"
	echo 'pm-suspend' | sudo at now + $minutes minutes
 elif [ "x$action" = "xhalt" ]; then
	echo "halt in $minutes m"
	echo 'halt' | sudo at now + $minutes minutes
elif [ "x$action" = "xhibernate" ]; then
	echo "hibernate in $minutes m"
	echo 'pm-hibernate' | sudo at now + $minutes minutes
elif [ "x$action" = "xshutdown" ]; then
	echo "shutdown in $minutes m"
	echo 'shutdown +0' | sudo at now + $minutes minutes
elif [ "x$action" = "xcancel" ]; then
	echo "Removing "sudo at -l
	sudo at -r $(sudo at -l | cut -f1)
else
	help;
fi


