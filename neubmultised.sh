#!/bin/sh
#
# Multi Sed: replace various pattern in a file at the same time.
#
# Copyright (c) 2013, Benoit RAT
# Authors: Benoit RAT <benoit(AT)sevensols.com>
# Licence: GPL v2 or later.
# Website: http://www.neub.co.nr
#
######################################################

## Default value
expr_sep=" => "
sedkey='#'
inplace=''
rtrim="%%%%%%%%%%%"

multi_sed()
{

_exprfile="$1"
sep="$2"
sedchar="$3"
iofile="${4}"
_rtrim="${5}"

echo "cat ${_exprfile} | grep ${sep} | sed \"s@^${_rtrim}@@g\""
cat ${_exprfile} | grep ${sep} | sed "s@^${rtrim}@@g" > /tmp/tmpA
cat /tmp/tmpA | \
while read line; do
	#line=${line%?}
	find=$(echo ${line} | awk -F''"${sep}"'' '{ print $1}')
	replace=$(echo ${line} | awk -F''"${sep}"'' '{ print $2}')
	
	if [ "x$iofile" = "x" ]; then
		echo "${line} >> s${sedchar}${find}${sedchar}${replace}${sedchar}"
	else
		echo "sed -i "\'s${sedchar}${find}${sedchar}${replace}${sedchar}\'" ${iofile}"
		sed -i "s${sedchar}${find}${sedchar}${replace}${sedchar}" ${iofile}
	fi
done

}


help() 
{
cat << EOF
Usage: $(basename $0)  [OPTIONS] <expr> <infile>

OPTIONS:
   --sep <pattern>        : Grep and Separator to retrieve sed expression (default=${expsep})
   --sed_char  <char>     : Key char used by sed (by default=${sedkey})
   --rtrim <patten>	  : Usually used to remove comment at beggining of a line when sed pattern is embbeded in the file (i,e --rtrim=#)
   -i|--inplace           : Inplace modification (be default we use <infile>.sed
   -h|--help              : Show this help
       
EOF
exit 0
}

## Parse options
while [ $# -gt 0 ]; do    # Until you run out of parameters . . .
	echo "1 $1, 2 $2"	
	case "$1" in
		--sep) expr_sep="$2"; shift;;
		--sedkey) sedkey="$2"; shift;;
		--rtrim) rtrim="$2"; shift;;
		-i|--inplace) inplace=1; break;;
		-h|--help) help;;
		-*) echo "Bad args $1"; help;;
		*) break;;
	esac
	shift       # Check next set of parameters.
done

## Parse arguments
exprfile="$1"
ifile="${2}"
ofile="${2}.sed"

## Run test script
multi_sed "$exprfile" "${expr_sep}" "${sedkey}" '' "${rtrim}"

## Run sed
echo "${exprfile}: ${ifile}>${ofile}"
read -p "continue processing sed on file ${outfile}? [y/N] :  " valid;
if [ "x$valid" = "xy" ]; then
	cp "${ifile}" "${ofile}"
	multi_sed "$exprfile" "${expr_sep}" "${sedkey}" "${ofile}"
	tail -15 "${ofile}"
fi
