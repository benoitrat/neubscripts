#!/bin/bash
#
# Merge PDF using pdfk
#
# Copyright (c) 2013, Benoit RAT
# Authors: Benoit RAT <benoit(AT)sevensols.com>
# Licence: GPL v2 or later.
# Website: http://www.neub.co.nr
#
#######################


### Show help of the function
help () 
{
cat << EOF
Usage: $(basename $0)  [Options] <pdf1> <pdf2> ... <pdf9>
	
Options:	
	-?|-h|--help     	                    : Show this little help
	-o|--output <outfname>    : The merging pdf will be written in outfname otherwise it will be written in MERGED.pdf
	
EOF
exit 0
}

output=MERGED.pdf

### Check option
while [ $# -gt 0 ]; do    # Until you run out of parameters . . .
	case "$1" in
	-?|--help)     	 	help;;
	-o|--ouput) 	output=$2; shift;;
	-*) echo "Bad option $1"; help; exit;;
	*) break;;
	esac
	shift       # Check next set of parameters.
done

### Check input files

pdftk $1 $2 $3 $4 $5 $6 $7 $8 $9 cat output ${output}
