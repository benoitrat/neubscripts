#!/bin/bash
###########################################
# 
# Easily kill process by name (advanced killall).
#
# Copyright (c) 2012, Benoit RAT
# Authors: Benoit RAT <benoit(AT)sevensols.com>
# Licence: GPL v2 or later.
#
#
###########################################


########################################################
# Describe functions
ask2_continue() {
	
	echo ""
	read -p "$continue_msg [y/a/N] :  " conti;

	if [[ $conti != "y" ]]; then
		if [[ $conti != "a" ]]; then
			echo -e ""
			echo -e "Stop the script"
			exit 0
		fi
	fi
}


########################################################

SIG=9
if [ "$1" == "" ]; then
    echo "Usage: $0 [process name]"
    exit
fi


echo "ps -Af | grep \"$1\""
ps -aef | grep "$1" | grep -v grep

continue_msg="Do you want to kill some process?"
ask2_continue;

plist=`ps -aef | grep "$1" | grep -v grep | awk '{print $2}'`

for PID in $plist; do
	if [ "$conti" != "a" ]; then
		read -p "kill -$SIG $PID. confirm? [y/N] :  " valid;
	else
		valid="y";
	fi
	if [ "$valid" == "y" ]; then
		kill -$SIG $PID
		echo "$PID killed!"
	fi 
	
done


