#!/bin/bash
###########################################
# 
# Grep the history and clip it using xclip tool.
#
# Copyright (c) 2011, Benoit RAT
# Authors: Benoit RAT <benpaka.spam(AT)gmail.com>
# Licence: GPL v2 or later.
# Dep: xclip
#
#
###########################################



########################################################
# Describe functions
ask2_continue() {
	
	echo ""
	read -p "$continue_msg [y/N] :  " valid;

	if [ "$valid" != "y" ]; then
		echo -e ""
		echo -e "Stop the script"
		exit 0
	fi
}


########################################################

SIG=9
if [ "$1" == "" ]; then
    echo "Usage: $0 [pattern]"
    exit
fi


echo "history | grep \"$1\""

HISTFILE=~/.bash_history
set -o history
history | grep "$1"


read -p "Select a line number to copy: " number;

if [ "$number" -gt "0" ]; then
	ret=`history | grep "$1" | grep "$number" | grep -v grep | nawk '{$1="";$0=substr($0,2)}1'`
	echo "$ret" | xclip -selection clipboard
	echo "$ret" | xclip
	echo "\"$ret\" is copied to clipboard"

fi

