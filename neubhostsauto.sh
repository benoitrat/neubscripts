#!/bin/bash
## Script to auto update /etc/hosts files
##
## If your entry in /etc/hosts are in the following format:
##
## ~~~~~~~~~~
## 172.17.5.148 llrf0  #00:c0:88:01:10:c8
## 172.17.5.150 llrf1  #00:c0:88:01:10:c0
## ~~~~~~~~~~
##
## This script will retrieve all the mac address using nmap
## and update the IP associated to this mac in the /etc/hosts
##
## Authors: 
##	- Benoit Rat (Seven Solutions, www.sevensols.com)
##
## GNU Lesser General Public License Usage
## This file may be used under the terms of the GNU Lesser
## General Public License version 2.1 as published by the Free Software
## Foundation and appearing in the file LICENSE.LGPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU Lesser General Public License version 2.1 requirements
## will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
#######################################################################################################

showhelp()
{
  echo "$0 [-u] <hostname>"
  exit 0
}

hname=$1

case "$1" in
  "--help"|"-h"|"help") showhelp;;
  "-u") nmap -sP 172.17.5.0/24; hname=$2;;
  "") showhelp;;
esac

if [ "x$hname" = "x" ]; then
	echo "Argument hostname is empty"
	showhelp
fi


mac=$(cat /etc/hosts | grep $hname | cut -d"#" -f2)
if [ "x$mac" = "x" ]; then
	echo "error: could not find any MAC associate to '$hname'"
	cat /etc/hosts | grep ${hname:0:3}
	exit 0
fi

if [ $(echo $mac | wc -w) -gt 1 ]; then
	echo "warning: multiple MACs found using '$hname' pattern, we use the first one"
	cat /etc/hosts | grep ${hname}
	mac=$(cat /etc/hosts | grep $hname | head -1 | cut -d"#" -f2)
fi

ip=$(arp -n | grep -i $mac | cut -d" " -f1)
echo $ip

if [ "x$ip" = "x" ]; then
	echo "error: No IP associate with MAC=$mac ($hname)"
	echo "Please try to run with the update option (e.g, $(basename $0) -u $hname)"
	exit 0
fi


cat /etc/hosts | sed "s/[0-9\.]*\(.*#$mac\)/$ip\1/"  > /tmp/hosts
diff /etc/hosts /tmp/hosts
if [ "x$?" = "x0" ]; then
	echo "already up to date"
	exit 0
fi

read -p "Do you want to overwrite /etc/hosts? [y/N]: " ret
if [ "x$ret" = "xy" ]; then
	sudo cp /tmp/hosts /etc/hosts
fi
