#!/bin/bash
#
# Improve the grep with file extension
#
# Copyright (c) 2011, Benoit RAT
# Authors: Benoit RAT <benoit(AT)sevensols.com>
# Licence: GPL v2 or later.
# Website: http://www.neub.co.nr
#
#######################

extensions="h hpp hxx c cxx cpp java vhdl vhd html php js m sh py lua tex md txt log ucf"

### Show help of the function
help () 
{
cat << EOF
Usage: $(basename $0)  [Options] <pattern> [<folder/fileype>]
	
Options:	
	-h|-?|--help)     	 Show this little help

Default extensions are:
${extensions}
EOF
exit 0
}

### Check first arguments
case "x$1" in
	x-h|x--help|x-?) help;;
	x)	echo "no args"; help;;
esac
pattern=$1


### Check second arguments
folder=$(dirname ${2:-"."})
ftype=$(basename ${2:-""} | sed 's/.*\.\(.*\)/\1/')

echo "inputs=${folder},${ftype}"
if [ "x${ftype}" != "x" ]; then
	extensions="${ftype}"
fi

args="--include=*.{"
for ext in ${extensions}; do
	args="${args}${ext},"	
done
args="${args}}" 


### Run command
cmd="grep -ni --color=auto ${args} $pattern -R $folder"
echo "$cmd"
eval "${cmd}"

