#!/usr/bin/env perl

my $person_key = shift;
my $dir_name = '~/tmp';
if(@ARGV) {
       $dir_name = shift;
}

$person_key = lc($person_key);
my @person = split(/\+/,$person_key);
my $person_name = '';
foreach my $item_name (@person) {
       $person_name .= ucfirst($item_name);
}
$dir_name .= "/$person_name";
mkdir($dir_name);

my $c1 = "wget --dns-timeout=60 --connect-timeout=60 --read-timeout=60 -l 1 -U NoSuchBrowser/1.0 -t 4 ";
my $c2 = "'images.google.fr/images?q=$person_key&ndsp=20&svnum=10&hl=it&lr=&start=";
my $c3 = "&sa=N' -O $dir_name/imtmp.html";

my $htmp_file = "$dir_name/images.html";
if(-f $htmp_file) {
       system("rm $htmp_file");
}

for($i=0; $i<30; $i++) {
       my $step = 20;
       my $val = $i * $step;
       my $command = "$c1$c2$val$c3";
       system($command);
       system("cat $dir_name/imtmp.html >> $htmp_file");
}

my @jpg_files;

my $strfile;

open(FIN,'<',$htmp_file);

while(<FIN>) {
       chomp;
       if(m/imgurl=http:\/[\/a-zA-Z0-9\.%_-]+\.jpg/) {
               foreach my $url (split(/imgurl=/)) {
                       if($url=~m/^http:\/[\/a-zA-Z0-9\.%_-]+\.jpg/) {
                               $url =~ s/^(http:\/[\/a-zA-Z0-9\.%_-]+\.jpg).*$/$1/;
                               push @jpg_files,$url;
                       }
               }
       }
}

close(FIN);

@jpg_files = sort(@jpg_files);

my $jpgprev = "";
foreach my $jpg (@jpg_files) {
       if($jpg ne $jpgprev) {
               print("$jpg\n");
               system("cd $dir_name; $c1 '$jpg'");
               $jpgprev = $jpg;
       }
}


#!/bin/sh

find $1 -name '*.jpg*' -print | grep '.jpg[\.0-9]*$' > $1/Photo.list
find $1 -name '*.jpeg*' -print | grep '.jpeg[\.0-9]*$' >> $1/Photo.list

matlab -nojvm -nodisplay -r "PhOrgPhase1('$1','$2'); exit;"
