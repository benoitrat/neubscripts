#!/usr/bin/env perl

my $file = "playlist.m3u";
my $searchrep = "/";

################################
####    Functions Tools     ####
# $_[X] correspond a l'argument X (1,2,...) d'une function
 

# Check File Exist 
sub check_file {
	if (-f $_[0]){
		print "$_[0] is found.\n";
		#return 1
	}
	else {
		print "$!\n";
		exit 0;
	}
}

sub check_path {
	my $dir;
	#Reformat dir name
	if($_[0] == ".") {
		$dir = `pwd`;
	}
	else {
		$dir = `pwd`/$_[0];
	}
	chomp($dir);
	#Check dir existance
	if (-d $dir){
		print "$dir is found.\n";
		return $dir 
	}
	else {
		print "search $dir error: $!\n";
		exit 0;
	}
}


sub remove_path {
	###Split the /
	split("/",$_[0]);
	###count the number of split there is in the file
	$nb = @_;
	return $_[$nb-1];
}

sub remove_ext {
	###Remove the extension
	split(/\./,$_[0]); 
	return $_[0];
}

	

#### End Functions Tools

if(@ARGV) {
	#print "La 4ème valeur du tableau est $ARGV[3]
	$file = shift;
	$rep=shift;
	print("- m3u file:$file\n- Research dir:$searchrep\n");
	system("echo entering the scripts");

}
else {
	print("m3u filename to update\n"); 
	#We wait for an input on standard input
	$file = <STDIN>;
	#To quit the return character use chomp;
	chomp $file;
	print("$saisie");
} 

check_file($file);
$searchrep=check_path($rep);
$fwrite=remove_ext($file);
$fwrite="$fwrite-updated.m3u";

print "new file is $fwrite.\n";
print("\n\n");

open(FID,'<',$file) or die("File $file not found\n");
open(FWRITE,'>', $fwrite) or die("File: $fwrite not created\n");
my $nb_err=0;
my $nb_total=0;
while($line = <FID>) {
	chomp($line);
	chop($line);
	if($line =~ /#EXTINF:/) {
		split(/,/,$line);
		$artitle=$_[1];
		#Change all non accent by ? 
		#This expression mean all char excepted (^): space (\s),alphanum (\w), _,- 
		$artitle=~ s/[^\s\w-_.]/\?/g;

		$result = `find $searchrep -iname \"$artitle*\"`;
		if(not($result)) {
			print("error in : find $searchrep -iname \"$artitle*\"\n");
			$nb_err=$nb_err+1;
		}
		else{
			chomp($result);
			print FWRITE "$line\n$result\n";
		}
	$nb_total=$nb_total+1;	
       }
}
close(FWRITE);
close(FID);
$_ = int($nb_err*100/$nb_total);
print("Total files processed: $nb_total\nErrors files: $nb_err ($_%)\n");
