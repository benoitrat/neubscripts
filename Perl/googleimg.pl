#!/usr/bin/env perl

my $file = "images.html";
my @jpg_files;

system("echo entering the scripts");

if(@ARGV) {
       $file = shift;
}

my $strfile;

open(FIN,'<',$file);

while(<FIN>) {
       chomp;
       if(m/imgurl=http:\/[\/a-zA-Z0-9\.%_-]+\.jpg/) {
               foreach my $url (split(/imgurl=/)) {
                       if($url=~m/^http:\/[\/a-zA-Z0-9\.%_-]+\.jpg/) {
                               $url =~ s/^(http:\/[\/a-zA-Z0-9\.%_-]+\.jpg).*$/$1/;
                               push @jpg_files,$url;
                       }
               }
       }
}
#For FlickR
#http://static.flickr.com/5/7176730_ec905d9e1a_m.jpg > http://static.flickr.com/5/7176730_ec905d9e1a.jpg


close(FIN);

@jpg_files = sort(@jpg_files);


#my $proxy = "--proxy=on --proxy-user='username' --proxy-password=password";
my $proxy = "";
my $jpgprev = "";

foreach my $jpg (@jpg_files) {
       if($jpg ne $jpgprev) {
               print("$jpg\n");
               system("wget $proxy '$jpg'";
               $jpgprev = $jpg;
       }
}
