﻿#target photoshop
/**
  *  Save each group layers as an image for the web.
  * 
  *  - All layers group are hidden except the one named: background or starting by "0o." 
  * - The group name is used as part of the the filename
  * - When asking prefix for the filename you can add %02d or %d to use the index of layer group. 
  * - The first layer name in a group can be used as metadata description/title 
   * - The visible/hidden layers inside a group are not touched (only the group)
  **/

function main(isSaveWEB){
	if(!documents.length) return;
	var doc = activeDocument;
	var oldPath = activeDocument.path;
	var prefix= doc.name.substring(0, doc.name.indexOf('.'));
	var desc="";
	
	if(isSaveWEB)
	{

        
        var dlg = new Window("dialog{text:'Save Group Layers as Web',bounds:[100,100,460,320],\
		statictext0:StaticText{bounds:[10,10,111,27] , text:'max_size' ,properties:{scrolling:undefined,multiline:undefined}},\
		ETmaxsize:EditText{bounds:[130,10,350,26] , text:'-1' ,properties:{multiline:false,noecho:false,readonly:false}},\
		statictext1:StaticText{bounds:[10,30,111,47] , text:'quality' ,properties:{scrolling:undefined,multiline:undefined}},\
		ETquality:EditText{bounds:[130,30,350,46] , text:'80' ,properties:{multiline:false,noecho:false,readonly:false}},\
		statictext2:StaticText{bounds:[10,50,111,67] , text:'type' ,properties:{scrolling:undefined,multiline:undefined}},\
		DDtype:DropDownList{bounds:[130,50,350,65], properties:{items:['jpg','png8','png24']}},\
		statictext3:StaticText{bounds:[10,70,111,87] , text:'prefix' ,properties:{scrolling:undefined,multiline:undefined}},\
		ETprefix:EditText{bounds:[130,70,350,86] , text:'"+prefix+"_%02d' ,properties:{multiline:false,noecho:false,readonly:false}},\
		buttonKO:Button{bounds:[140,180,240,200] , text:'Cancel' },\
		buttonOK:Button{bounds:[250,180,350,200] , text:'OK' }\
};");
		dlg.DDtype.items[ 0 ].selected = true;     
        var retdlg = dlg.show();
        
        
        if(retdlg==1)
        {
        	    var maxSize = dlg.ETmaxsize.text;
                var quality = dlg.ETquality.text;
                var type = dlg.DDtype.selection.text;
                var prefix = dlg.ETprefix.text;
        }
        else return;
        
	}
	
	var newDoc = doc.duplicate("duplicate "+doc.name);
	
	//Hide all layers except  background and the one starting by 0o.
	for(var a=0;a<newDoc.layerSets.length;a++){
		if(newDoc.layerSets[a].name=="background") continue;
		if(doc.layerSets[a].name.substring(0,3)=="0o.") continue;
		newDoc.layerSets[a].visible=false;
	}
	
	for(var a=0;a<newDoc.layerSets.length;a++){
		if(doc.layerSets[a].name=="background") continue;
		if(doc.layerSets[a].name.substring(0,3)=="0o.") continue; 
		if(doc.layerSets[a].layers.length==0) continue; 		//If no layer in the subgroup we skip it

        //Set this layer to visible
        newDoc.layerSets[a].visible=true;
		
		if(doc.layerSets[a].layers[0].name.substring(0,5)!="Layer")
			desc=doc.layerSets[a].layers[0].name;
		
		//Bad hack to add number in the script
		var prefixa=prefix.replace("%d",a);
		if(a>10) prefixa=prefixa.replace("%02d",a);		
		else prefixa=prefixa.replace("%02d","0"+a);
		
		var fileName=oldPath +"/"+prefixa+"-"+doc.layerSets[a].name.replace(" ","_");
	
    
        //Check Metadata
        var n_info=newDoc.info;
        var o_info=doc.info;
        
        setExif(doc.info,"ImageDescription",270,desc);  
        setExif(doc.info,"Software",305,"JSX save from group layer");
        setExif(doc.info,"UserComment",37510,doc.info.title);     

        
        if(desc != "") 	{
            newDoc.info.title=desc;
            newDoc.info.captionWriter=desc;
            newDoc.info.headline=desc;
            newDoc.info.caption=desc;
    	}


		if(isSaveWEB)
		{
			saveWEB(newDoc,fileName, quality, maxSize, type)
		}
		else
		{
			saveJPEG(newDoc,fileName+".jpg", 12);
		}
		newDoc.layerSets[a].visible=false;
	}
	
	newDoc.close(SaveOptions.DONOTSAVECHANGES);
}


function setExif(doc_info,key,id,data)       
{
        for (var i = 0; i < doc_info.exif.length; i++)
        {
                   if(doc_info.exif[i][0] == key || doc_info.exif[i][2]==id)
                   {
                       doc_info.exif[i][1]=data;
                       return;
                    }            
         }
         //Append new value
         doc_info.exif.push([key,data,id]);
} ;
          


function dupLayers(layerToDup) { 
	var desc143 = new ActionDescriptor();
	var ref73 = new ActionReference();
	ref73.putClass( charIDToTypeID('Dcmn') );
	desc143.putReference( charIDToTypeID('null'), ref73 );
	desc143.putString( charIDToTypeID('Nm  '), layerToDup.name );
	var ref74 = new ActionReference();
	ref74.putEnumerated( charIDToTypeID('Lyr '), charIDToTypeID('Ordn'), charIDToTypeID('Trgt') );
	desc143.putReference( charIDToTypeID('Usng'), ref74 );
	executeAction( charIDToTypeID('Mk  '), desc143, DialogModes.NO );
};

function saveJPEG(document,fileName, jpegQuality){
	jpgSaveOptions = new JPEGSaveOptions();
	jpgSaveOptions.embedColorProfile = true;
	jpgSaveOptions.formatOptions = FormatOptions.STANDARDBASELINE;
	jpgSaveOptions.matte = MatteType.NONE;
	jpgSaveOptions.quality = jpegQuality;
	var saveFile= new File(fileName);
	document.saveAs(saveFile, jpgSaveOptions, true,Extension.LOWERCASE);
}


function saveWEB(document,fileName, quality, maxSize, type)
{

	if ((maxSize > 0 ) && (document.width > maxSize || document.height > maxSize)) {
		if (document.width > document.height) {
			var newWidth = new UnitValue (maxSize + " pixels");
			var newHeight = new UnitValue (document.height*(maxSize/document.width) + " pixels");
		} else {
			var newWidth = new UnitValue (document.width*(maxSize/document.height) + " pixels");
			var newHeight = new UnitValue (maxSize + " pixels");
		}
		document.resizeImage (newWidth, newHeight, quality);
	}
	exportOptions = new ExportOptionsSaveForWeb ();
	if(type.substring(0,3)=="png")
	{
		exportOptions.format = SaveDocumentType.PNG;
		fileName+= ".png";
		if(type=="png24") exportOptions.PNG8=false;

	}
	else
	{
		exportOptions.format = SaveDocumentType.JPEG;
		fileName+= ".jpg";
	}
	exportOptions.quality = quality;
     
	var saveFile= new File(fileName);
	document.exportDocument (saveFile, ExportType.SAVEFORWEB, exportOptions);
}


main(true);

