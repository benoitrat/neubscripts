#target photoshop
function main(isSaveWEB){
	if(!documents.length) return;
	var doc = activeDocument;
	var oldPath = activeDocument.path;
	
	if(isSaveWEB)
	{
		var maxSize = prompt ("Please choose the maximum size (-1 do nothing)", "-1");
		var quality = prompt ("Please choose the quality:", "80");
		var type = prompt ("Please choose the extension:", "jpg");
	}
	
	var newDoc = doc.duplicate("duplicate "+doc.name);
		
	for(var a=0;a<newDoc.layerSets.length;a++){
		if(newDoc.layerSets[a].name=="background") continue;
		if(doc.layerSets[a].name.substring(0,3)=="0o.") continue;
		newDoc.layerSets[a].visible=false;
	}
	
	for(var a=0;a<newDoc.layerSets.length;a++){
		if(doc.layerSets[a].name=="background") continue;
		if(doc.layerSets[a].name.substring(0,3)=="0o.") continue;
		
		newDoc.layerSets[a].visible=true;
	
		var fileName=oldPath +"/"+doc.name.substring(0, doc.name.indexOf('.'))+"-"+doc.layerSets[a].name.replace(" ","_");
	
	
		if(isSaveWEB)
		{
			saveWEB(newDoc,fileName, quality, maxSize, type)
		}
		else
		{
			saveJPEG(newDoc,fileName+".jpg", 12);
		}
		newDoc.layerSets[a].visible=false;
	}
	
	newDoc.close(SaveOptions.DONOTSAVECHANGES);
}

function dupLayers(layerToDup) { 
	var desc143 = new ActionDescriptor();
	var ref73 = new ActionReference();
	ref73.putClass( charIDToTypeID('Dcmn') );
	desc143.putReference( charIDToTypeID('null'), ref73 );
	desc143.putString( charIDToTypeID('Nm  '), layerToDup.name );
	var ref74 = new ActionReference();
	ref74.putEnumerated( charIDToTypeID('Lyr '), charIDToTypeID('Ordn'), charIDToTypeID('Trgt') );
	desc143.putReference( charIDToTypeID('Usng'), ref74 );
	executeAction( charIDToTypeID('Mk  '), desc143, DialogModes.NO );
};

function saveJPEG(document,fileName, jpegQuality){
	jpgSaveOptions = new JPEGSaveOptions();
	jpgSaveOptions.embedColorProfile = true;
	jpgSaveOptions.formatOptions = FormatOptions.STANDARDBASELINE;
	jpgSaveOptions.matte = MatteType.NONE;
	jpgSaveOptions.quality = jpegQuality;
	var saveFile= new File(fileName);
	document.saveAs(saveFile, jpgSaveOptions, true,Extension.LOWERCASE);
}


function saveWEB(document,fileName, quality, maxSize, type)
{

	if ((maxSize > 0 ) && (document.width > maxSize || document.height > maxSize)) {
		if (document.width > document.height) {
			var newWidth = new UnitValue (maxSize + " pixels");
			var newHeight = new UnitValue (document.height*(maxSize/document.width) + " pixels");
		} else {
			var newWidth = new UnitValue (document.width*(maxSize/document.height) + " pixels");
			var newHeight = new UnitValue (maxSize + " pixels");
		}
		document.resizeImage (newWidth, newHeight, quality);
	}
	exportOptions = new ExportOptionsSaveForWeb ();
	if(type=="png" || type =="PNG")
	{
		exportOptions.format = SaveDocumentType.PNG;
		fileName+= ".png"
	}
	else
	{
		exportOptions.format = SaveDocumentType.JPEG;
		fileName+= ".jpg"
	}
	exportOptions.quality = quality;
	var saveFile= new File(fileName);
	document.exportDocument (saveFile, ExportType.SAVEFORWEB, exportOptions);
}

main(false);

