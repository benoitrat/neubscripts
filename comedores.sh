#!/bin/bash

# Script para mostrar el menu del comedor de la Universidad de Granada
# 
# Se puede añadir una tarea cron para facilitar su uso:
#
# 	55 13,18 * * 1-6 <pathtoscript>/comedores.sh gui




fecha=$(date -d +9hours +%A)

if  [ $fecha = "Monday" ]; then
	dia="lunes"
elif [ $fecha = "Tuesday" ]; then
	dia="martes"
elif [ $fecha = "Wednesday" ]; then
	dia="miercoles"
elif [ $fecha = "Thursday" ]; then
	dia="jueves"
elif [ $fecha = "Friday" ]; then
	dia="viernes"
elif [ $fecha = "Saturday" ]; then
	dia="sabado"
elif [ $fecha = "miércoles" ]; then
	dia="miercoles"
else
	dia=$fecha
fi


# Descargamos el menú
wget -q http://comedoresugr.tcomunica.org/ --output-document=/tmp/comedores.html > /dev/null
iconv -f ISO-8859-1 -t utf-8 /tmp/comedores.html > /tmp/comedores2.html

##Obtain user of the script
user=$(ls -l $0 | awk '{print $3}')
echo "$1 $dia $(date)"
echo "$(date -d +9hours +%d) $dia" 

#echo Select near which zone 
echo "cat /tmp/comedores2.html | grep -A 15 -i $dia | grep -A 12 "$(date -d +9hours +%d)" | sed 's/\s\{2,\}//g' | tr -d '\n' > /tmp/comedores3.html"
cat /tmp/comedores2.html | grep -A 15 -i $dia | grep -A 12 "$(date -d +9hours +%d)" | sed 's/\s\{2,\}//g' | tr -d '\n' > /tmp/comedores3.html


# Obtenemos el menú del día
plato1=$(cat /tmp/comedores3.html | sed -n 's/.*plato1">\(.[^<]*\).*/\1/p')
plato2=$(cat /tmp/comedores3.html | sed -n 's/.*plato2">\(.[^<]*\).*/\1/p') 
plato3=$(cat /tmp/comedores3.html | sed -n 's/.*plato3">\(.[^<]*\).*/\1/p')


# Mostramos el menú
if [[ $1 == "gui" || $1 == "--gui" ]]; then
	DISPLAY=:0.0 sudo -u ${user} notify-send -i /home/neub/Dropbox/Misc/Icons/rest.png "$dia:" "* $plato1
* $plato2
* $plato3"

else
	echo "Menú para el $dia:"
	echo $plato1
	echo $plato2
	echo $plato3
fi

#Borramos archivo descargado
rm /tmp/comedores.html
#rm /tmp/comedores2.html
