#!/bin/sh
#
# Add a random prefix on mp3 file when your mp3 player doesn't provide
# a good random function.
#
# Copyright (c) 2011, Benoit RAT
# Authors: Benoit RAT <benoit(AT)sevensols.com>
# Licence: GPL v2 or later.
# Website: http://www.neub.co.nr
#
#######################



### Show help of the function
help () 
{
cat << EOF
Usage: $(basename $0)  <folder>

        
EOF
exit 0
}

folder="$1"


cd "$folder"
for input in *.mp3; do
	folder=$(basename $folder)
	rprefix=$(seq -w 0 999 | shuf -n 1)
	output="RAN${rprefix}DOM_${folder}__${input}"
	echo $output
	mv "$input" "$output" 
done


