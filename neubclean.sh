#!/bin/sh
## Script to clean and change persmission in the folder in an efficient way.
##
## Authors: 
##	- Benoit Rat (Seven Solutions, www.sevensols.com)
##
## GNU Lesser General Public License Usage
## This file may be used under the terms of the GNU Lesser
## General Public License version 2.1 as published by the Free Software
## Foundation and appearing in the file LICENSE.LGPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU Lesser General Public License version 2.1 requirements
## will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
#######################################################################################################

ask2continue()
{
	printf "$1 [%s] ? " $2 
	read YN

	case "$2=$YN" in
		y/N=[yY]) return 0;;
		y/N=*) return 1;;
		Y/n=[nN]) return 1;;
		Y/n=*) return 0;;
		[yY]/[nN]=[yY]) return 0;;
		[yY]/[nN]=[nN]) return 1;;
		[yY]/[nN]=*) echo "$YN is not a valid value";  ask2continue "$1" "$2"; return $?;;
		*) echo "Argument 2 is not valid [Y/n, y/N or Y/N]"; return 1;;
	esac;
}

# args : as2kcontinue text, ask2continue behaviour, file to check
ask2contfile()
{
	if [ $(wc -w "$3" | cut -d" " -f1) -gt 0 ]; then
		cat "$3" | xargs -0 printf "%s\n" 
		ask2continue "$1" "$2"
		return $?
	else
		echo "Nothing to clean"
		return 1
	fi
}


owner()
{
	egrep -i "^${2}:" /etc/passwd &> /dev/null
	if [ $? = "0" ]; then
		echo "Setting owner $2 in $1"
		cd $1; chown -R $2 *
	else
		echo "$2 is not valid user"
	fi
}


group()
{
	egrep -i "^${2}:" /etc/group &> /dev/null
	if [ $? = "0" ]; then
		echo "Setting group $2 in $1"
		cd $1; chgrp -R $2 *
	else
		echo "$2 is not valid group"
	fi
}

##file 644, directory to 755, scripts to 754 
permission()
{
	
	echo "Change permission in $1"
	find $1 -type f -print0 | xargs -0 chmod 644
	find $1 -type d -print0 | xargs -0 chmod 755
	find $1 -type f -print0 | xargs -0 file | grep script | cut -d":" -f1 | sed 's/ /\\ /g'  > /tmp/fileexec
	cat /tmp/fileexec | xargs chmod -v 754
	exit 0
}

temporary()
{
	
	echo "Remove temporary file in $1"
	##find $1 -iname "*~" -print | sed 's/ /\\ /p' 
	find $1 -type f \( -name "*~" -or -name "*.tmp" -or -name "*swp" -or -name "*.orig" \) -print0 > /tmp/files
	ask2contfile "Are you sure you want to delete these files" "y/N" /tmp/files
	if [ $? = "0" ]; then
		echo "Deleting..."
		cat /tmp/files | xargs -0 rm -v
	fi
	exit 0
}


tspace()
{
	echo "Remove trailing space in directory $1"
	find $1 -type f \( -name "*.c" -or -name "*.h" -or -name "*.sh" \) -exec sed --in-place 's/[[:space:]]\+$//' {} \+
}


svn()
{
	echo "Cleaning .svn files in folder $1"
	find $1 -name .svn -print0 | xargs -0 rm -r
}

git()
{
        echo "Cleaning .git files in folder $1"
        find $1 -name .git -print0 | xargs -0 rm -r
}


dupli()
{
	echo "Remove temporary file in $1"
	fdupes  Downloads/ | grep "(.*)" > /tmp/files
	ask2contfile "Are you sure you want to delete these files" "y/N" /tmp/files
        if [ $? = "0" ]; then
                echo "Deleting..."
		cat /tmp/files | while read file; do rm -v "$file"; done
	fi
	exit 0
}


### Show help of the function
help () 
{
cat << EOF
Usage: $(basename $0)  [Options] 
	
Options:	
	-h|--help)     	 Show this little help
	-R )             Recursive option 
	-o|--owner)    	 Set owner (i.e, --owner neub)
	-g|--group)  	 Set groups (i.e, --group users)
	-p|--permission) Set file to 644, directory to 755, scripts to 754 
	-d|--duplicate)  Remove duplicated files (using fdupes)
	-t|--temporary)  Remove temporary files (~, .*swp, *.orig, ...)
	-u|--user)	 Same as -o ${USER} -g ${USER} -t -p
	-s|--space)      Remove trailing space 
	--git)      	 Clean git files
	--svn) 		 Clean svn files 
	

EOF
exit 0
}

#################################################
### Main execution

# setup script dir
scriptdir=$(cd $(dirname $0); pwd)
var_o=""
var_g=""


while [ $# -gt 0 ]; do    # Until you run out of parameters . . .
	case "$1" in
		-h|--help) help;;
		-R) recur_opt="-R";;
		-o|--owner) var_o=$2; shift;;
		-g|--group) var_g=$2; shift;;
		-p|--permission) flag_p=1;;
		-t|--temporary) flag_t=1;;
		-s|--space) flag_s=1;;
		-d|--duplicate) flag_d=1;;
		-u|--user) $0 -t -p -o ${USER} -g ${USER} $1; exit;;
		--svn) flag_svn=1;;
		--git) flag_git=1;;	
		-*) echo "Bad option $1"; help; exit;;
		*) break;;
	esac
	shift       # Check next set of parameters.
done

if [ -z $1 ]; then 
	echo "no given directory"
	help
fi

if [ ! -d $1 ]; then 
	echo "'${1}' is not a valid directory"
	exit
fi


if [ "x${flag_t}" = "x1" ]; then 
	temporary $1
fi

if [ "x${flag_p}" = "x1" ]; then 
	permission $1
fi

if [ ! "x${var_o}" = "x" ]; then 
	owner $1 ${var_o}
fi

if [ "x${var_g}" != "x"  ]; then 
	group $1 ${var_g}
fi

if [ "x${flag_s}" != "x"  ]; then 
	tspace $1
fi

if [ "x${flag_d}" != "x" ]; then
	dupli $1
fi

if [ "x${flag_svn}" != "x"  ]; then
        svn $1
fi

if [ "x${flag_git}" != "x" ]; then
        git $1
fi


