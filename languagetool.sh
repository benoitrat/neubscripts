#!/bin/sh
## Script process file with Languague tool software
##
## Authors: 
##	- Benoit Rat (Seven Solutions, www.sevensols.com)
##
## GNU Lesser General Public License Usage
## This file may be used under the terms of the GNU Lesser
## General Public License version 2.1 as published by the Free Software
## Foundation and appearing in the file LICENSE.LGPL included in the
## packaging of this file.  Please review the following information to
## ensure the GNU Lesser General Public License version 2.1 requirements
## will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
#######################################################################################################

LANGUATOOL_DIR=/opt/languagetool/1.9/
DRULES="WHITESPACE_RULE,EN_QUOTES,CURRENCY,UPPERCASE_SENTENCE_START,COMMA_PARENTHESIS_WHITESPACE"

help()
{
cat << EOF
Usage: $(basename $0)  <input> [<output>]

EOF
exit 0
}



input=$1
output=$2

if [ -z $input ]; then
	help;
fi

echo "Checking ${1}..."
java -jar ${LANGUATOOL_DIR}/LanguageTool.jar --disable $DRULES -l en ${input} > /tmp/output.txt 

if [ -z $output ]; then
	output=less
fi
gawk -f ${LANGUATOOL_DIR}/../stats.awk /tmp/output.txt | $output

